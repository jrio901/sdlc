package com.sdlc.jms.listener;

import static org.junit.Assert.assertNull;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConsumerListenerTest {
	private TextMessage message;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testOnMessage() {
		ConsumerListener cl = new ConsumerListener();
		cl.onMessage(message);
		assertNull(message);
	}

}
