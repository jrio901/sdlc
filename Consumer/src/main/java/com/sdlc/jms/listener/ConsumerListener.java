package com.sdlc.jms.listener;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.sdlc.jms.adapter.ConsumerAdapter;

@Component
public class ConsumerListener implements MessageListener {
	private static Logger log = LoggerFactory.getLogger(ConsumerListener.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	@Autowired
	private ConsumerAdapter consumerAdapter;
	
	
	@Override
	public void onMessage(Message message) {
		String json = null;
		
		log.debug("*** In onMessage()");	

		if ( message instanceof TextMessage ) {
			try {
				json = ((TextMessage) message).getText();
				log.debug("*** Sending JSON to DB: " + json);	
				consumerAdapter.sendToMango(json);
			} catch (JMSException e) {
				// Write to RME.QUEUE
				log.error("*** Message: " + json);	
				jmsTemplate.convertAndSend(json);
			}
		}

	}

}
