package com.sdlc.jms.adapter;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class ConsumerAdapter {
	private static Logger log = org.slf4j.LoggerFactory.getLogger(ConsumerAdapter.class);

	public void sendToMango(String json) {
		System.out.println("*** Sending to Mongo");
	}

}
